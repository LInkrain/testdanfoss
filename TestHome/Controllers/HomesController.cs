﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TestHome.Models;

namespace TestHome.Controllers
{
    public class HomesController : ApiController
    {
        HomeContext db = new HomeContext();

        [HttpGet]
        public async Task<IQueryable<Home>> GetHomes()
        {
            return await Task.Run(() => db.Homes.Include("HomeWaterMeter"));
        }

        [HttpGet]
        public Home GetHome(int id)
        {
            Home home = db.Homes.Include("HomeWaterMeter").First(x => x.Id == id);
            return home;
        }

        [HttpGet]
        public async Task<Home> HomeMaxIndicator()
        {
                return await Task.Run(() => db.Homes.Include("HomeWaterMeter").First(h1 => h1.HomeWaterMeter.Indicator == db.Homes.Max(h2 => h2.HomeWaterMeter.Indicator))); ;
        }

        [HttpGet]
        public async Task<Home> HomeMinIndicator()
        {           
            return await Task.Run(() => db.Homes.Include("HomeWaterMeter").First(h1 => h1.HomeWaterMeter.Indicator == db.Homes.Min(h2 => h2.HomeWaterMeter.Indicator)));
        }

        [HttpPost]
        public void CreateHome([FromBody]Home home)
        {
            db.Homes.Add(home);
            db.SaveChanges();
        }

        [HttpPost]
        public void AddWaterMeter([FromBody]WaterMeter waterMeter)
        {
            db.WaterMeters.Add(waterMeter);
            db.SaveChanges();
        }

        [HttpPut]
        public void EditHome(int id, [FromBody]Home home)
        {
            if (id == home.Id)
            {
                db.Entry(home).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public void DeleteHome(int id)
        {
            Home home = db.Homes.Find(id);
            if (home != null)
            {
                db.WaterMeters.Remove(db.WaterMeters.Find(id));
                db.Homes.Remove(home);
                db.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
