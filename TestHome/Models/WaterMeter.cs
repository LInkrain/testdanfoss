﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestHome.Models
{
    public class WaterMeter
    {
        [Key]
        [ForeignKey("Home")]
        public int Id { get; set; }
        public string FactoryNumber { get; set; }
        [Range(0, Double.MaxValue)]
        public double Indicator { get; set; }

        public Home Home { get; set; }
    }
}