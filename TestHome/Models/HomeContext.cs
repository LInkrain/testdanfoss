﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestHome.Models
{
    public class HomeContext : DbContext
    {
        public DbSet<Home> Homes { get; set; }
        public DbSet<WaterMeter> WaterMeters { get; set; }
    }
}