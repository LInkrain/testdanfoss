﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestHome.Models
{
    public class Home
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Index(IsUnique = true)]
        [StringLength(200)]
        public string Adress { get; set; }
        public WaterMeter HomeWaterMeter { get; set; }
        public Home()
        {
           
        }
    }
}