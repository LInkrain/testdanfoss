﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestHome.Models
{
    public class HomesDbInitializer: DropCreateDatabaseAlways<HomeContext>
    {
        protected override void Seed(HomeContext db)
        {
            Home home1 = new Home { Adress = "Таганская, дом 1А" };
            Home home2 = new Home { Adress = "Таганская, дом 2А" };
            Home home3 = new Home { Adress = "Таганская, дом 3А" };
            db.Homes.AddRange(new List<Home> { home1, home2, home3 });
            db.SaveChanges();
            WaterMeter meter1 = new WaterMeter { Id = home1.Id,  FactoryNumber = "23", Indicator = 232 };
            WaterMeter meter2 = new WaterMeter { Id = home2.Id, FactoryNumber = "42", Indicator = 422 };
            db.WaterMeters.AddRange(new List<WaterMeter> { meter1, meter2 });
            db.SaveChanges();
            base.Seed(db);
        }
    }
}